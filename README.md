# IGWN Computing Logo

This repo contains the source code (a short Python script) to generate the
IGWN Computing Logo. Documentation here:

<https://computing.docs.ligo.org/logo/>
