#!/usr/bin/env python3

from matplotlib import (
    rcParams,
    pyplot,
)

from pycbc.waveform import get_td_waveform

rcParams.update({
    "figure.subplot.bottom": 0.,
    "figure.subplot.top": 1.,
    "figure.subplot.left": 0.,
    "figure.subplot.right": 1.0,
})

# generate waveform
m = 200
hp, _ = get_td_waveform(
    approximant="IMRPhenomD",
    mass1=m,
    mass2=m,
    delta_t=1/16384.,
    f_lower=20,
)
start = -.2
end = .05
data = hp.crop(start-hp.start_time, hp.end_time-end) * 1e18

# plot the waveform
res = 1024
dpi = 256
inches = res / dpi
fig = pyplot.figure(figsize=(inches, inches))
ax = fig.gca()
ax.plot(data.sample_times, data, 'k-', linewidth=8, solid_capstyle='round')
ax.set_axis_off()
fig.tight_layout(pad=-0.5)
fig.savefig("igwn-logo.png", transparent=True)
